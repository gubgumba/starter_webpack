# Moved to http://github.com/mort3za/webpack-starter  

# Starter project  
Uses webpack, webpack-cli, webpack-dev-server, babel.  

## Commands  
`npm start` to open in browser in development mode.  
`npm dev` to run webpack once in development mode.  
`npm build` to build.  
